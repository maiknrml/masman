package com.packagename.masman;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;



@Route
@PWA(name = "Masman",
        shortName = "masman",
        description = "Masman - A Fitness Tracker.",
        enableInstallPrompt = false)
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
@CssImport("./styles/css/bootstrap.css")
public class MainView extends VerticalLayout {

	private static final long serialVersionUID = 1L;

    public MainView() {
    	
    	addClassName("main_view"); 
    	   	
    	H1 header= new H1("Masman");
    	add(header);
   	    	
    }


}






